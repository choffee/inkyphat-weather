# Simple weather forecast display

Licenced under the GPL, please read the COPYING file for more info.


## Using

Copy the myweather.ini.example to myweather.ini

Go get an openweather network API key. Free one should do but you can subscribe

pipenv run ./weather-phat.py --colour red


Right now it just does the local onscreen version but should run with InkyPhat
in the longer term


