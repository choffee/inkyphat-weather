#!/usr/bin/env python
"""Show the weather over the next three hours on an InkyPHAT"""
# -*- coding: utf-8 -*-

import glob
import json
import time
import urllib
import argparse
from inky import InkyPHAT
from PIL import Image, ImageDraw, ImageFont, ImageColor
#from font_fredoka_one import FredokaOne
from  pyowm import OWM
from pyowm.utils import timeutils
from io import BytesIO
import configparser

config = configparser.ConfigParser()
config.read('myweather.ini')

owmapikey = config['DEFAULT']['owmapikey']
location = config['DEFAULT']['location']

owm = OWM(owmapikey)
obs = owm.weather_at_place(location)


try:
    import requests
except ImportError:
    exit("This script requires the requests module\nInstall with: sudo pip install requests")

#print("""Inky pHAT: Weather

#Displays weather information for a given location. The default location is Sheffield-on-Sea.

#""")

# Command line arguments to set display colour


parser = argparse.ArgumentParser()
parser.add_argument('--colour', '-c', type=str, required=True, choices=["red", "black", "yellow"], help="ePaper display colour")
args = parser.parse_args()

# Set up the display

colour = args.colour
inky_display = InkyPHAT(colour)
inky_display.set_border(inky_display.BLACK)

#WHITE = ImageColor.getrgb('white')
#RED = ImageColor.getrgb('red')
#BLACK = ImageColor.getrgb('black')
#white=inky_display.WHITE
#red=inky_display.RED
WHITE=inky_display.WHITE
RED=inky_display.RED
BLACK=inky_display.BLACK


def get_weather(address):
    """ Get the weather at a given location and return some data"""
#    fc = owm.three_hours_forecast(address)
#    next_hour = timeutils.next_hour()
#    next_three_hours = timeutils.next_three_hours()
    observation = owm.weather_at_place(address)
    observation_values = observation.get_weather()
    #print(observation_values.get_humidity())
    #print(observation_values.get_rain())
    #print(observation_values.get_pressure())
    #print(observation_values.get_status())

    return simplify_weather(observation_values)

def simplify_weather(weather):
    """Takes an owm weather object and returns a simple summary"""
    if '3h' in weather.get_rain():
        rain = weather.get_rain()['3h']
    else:
        rain = 0
    return {
        "temperature": weather.get_temperature(unit='celsius')['temp'],
        "humidity": weather.get_humidity(),
        "rain": rain,
        "pressure": weather.get_pressure()['press'],
        "status": weather.get_status(),
        "icon": weather.get_weather_icon_name(),
        "time": weather.get_reference_time('date')
    }

def get_forecast_three_hours(address):
    """Get a forecast for the next three hours and return a simple dict."""
    seconds_in_hour = 60 * 60
    forecaster = owm.three_hours_forecast(address)
    forecast = forecaster.get_forecast()
    #print(forecast.to_JSON())
    for weather in forecast.get_weathers():
        #print(json.dumps(json.loads(weather.to_JSON()), sort_keys=True, indent=2 ))
        #print(weather.get_reference_time('iso'))
        #print(weather.get_rain())
        time = weather.get_reference_time('date')
        #print(time.month, time.day, time.hour, time.minute)
        if '3h' in weather.get_rain():
            rain = weather.get_rain()['3h']
            #print(rain)

    next_hour = timeutils.next_hour()
    next_three_hours = timeutils.next_three_hours()
    next_six_hours = forecaster.when_starts() + seconds_in_hour * 6
    next_nine_hours = forecaster.when_starts() + seconds_in_hour * 9
    simple_forecast = [
        simplify_weather(forecaster.get_weather_at(next_three_hours)),
        simplify_weather(forecaster.get_weather_at(next_six_hours)),
        simplify_weather(forecaster.get_weather_at(next_nine_hours)),
        ]
    return simple_forecast

def create_mask(source, mask=(WHITE, BLACK, RED)):
    """Create a transparency mask.

    Takes a paletized source image and converts it into a mask
    permitting all the colours supported by Inky pHAT (0, 1, 2)
    or an optional list of allowed colours.

    :param mask: Optional list of Inky pHAT colours to allow.

    """
    mask_image = Image.new("1", source.size)
    w, h = source.size
    for x in range(w):
        for y in range(h):
            p = source.getpixel((x, y))
            if p in mask:
                mask_image.putpixel((x, y), 255)

    return mask_image



forecast = get_forecast_three_hours(location)

img = Image.open("icons/backdrop.png")
draw = ImageDraw.Draw(img)

# Load the FredokaOne font
font = ImageFont.truetype("LiberationMono-Bold.ttf", 18)

# Write text with weather values to the canvas
datetime = time.strftime("%d/%m %H:%M")


def draw_weather(temperature, status_icon, precipitation, time, x_offset):
    x_pos = 5 + x_offset
    draw.line((x_pos - 2, 0, x_pos - 2, 104))
    draw.text((x_pos, 0), u"{:.1f}°".format(temperature), WHITE, font=font)
    icon_mask = create_mask(status_icon)
    img.paste(status_icon, (x_pos, 20))
    draw.text((x_pos, 60), u"{:.0f}mm".format(precipitation), WHITE, font=font)
    draw.text((x_pos, 80), u"{:s}".format(time), WHITE, font=font)

def image_to_three_colour(image):
    #img_three = image.quantize(colors=3)
    img_bw = image.convert('L')

    # This should probably be a pallet thing above.
    bw_image = Image.new('L',image.size)
    w, h = image.size
    for x in range(w):
        for y in range(h):
            pixel = img_bw.getpixel((x, y))
            if pixel <= 20:
                bw_image.putpixel((x,y), 1)
            elif pixel < 250:
                bw_image.putpixel((x,y), 0)
            else:
                bw_image.putpixel((x,y), 2)
    return bw_image

def make_icon_from_name(icon_name):
    """Take an icon URL and make an 3 color icon"""
    icon_url = "http://openweathermap.org/img/w/{}.png".format(icon_name)
    response = requests.get(icon_url)
    image = Image.open(BytesIO(response.content))
    return image_to_three_colour(image)

for num, weather in enumerate(forecast):
    time = "{:02d}:{:02d}".format(weather['time'].hour, weather['time'].minute)
    #print(weather)
    draw_weather(float(weather['temperature']), make_icon_from_name(weather['icon']), weather['rain'], time, 0+(70 * num))

# Display the weather data on Inky pHAT
inky_display.set_image(img)
inky_display.show()
#img.show()
